# Tumbex

Tumbex是一款使用Golang编写的Tumblr在线解析网站程序。

使用语言、程序、框架包括：

> 1. Golang
> 2. MongoDB
> 3. Beego
> 4. Bootstrap
> 5. Layer
> 6. Glyphicons

本程序遵循 [MIT License](https://mit-license.org/) 开源协议。

## 程序功能

本程序通过用户在解析页面输入`Tumblr`博客标识进行在线解析，解析后将博客数据保存至数据库。

博客解析过一次后，再次解析仅对比视频及图片文章数量是否相符，不相符重新解析获取文章，否则直接从数据库中获取文章数据。

使用文章标签与博客进行关联，在搜索处对标签进行搜索时，将获取到相关联的博客列表。

## 程序安装

> 1. 安装Golang
> 2. 安装MongoDB
> 3. 安装Git
> 4. 使用 `go get -u git.wskehao.com/ylqjgm/Tumbex` 拉取程序代码及相关库
> 5. 使用 `go get github.com/beego/bee` 安装`bee`工具
> 6. 到`Tumbex`代码目录下执行`bee run`

## 更新说明

### 2018.8.13

> 1. 将采集作为独立程序分开使用
> 2. 博客列表打开方式改为新窗口打开
> 3. 博客打开视频列表第一页时自动检查目标博客是否有新文章，有则更新
> 4. 修复解析方法逻辑错误问题
> 5. 其余问题修复

### 2018.8.9

> 1. 更换xml为json获取
> 2. 增加自动采集功能，仅使用官方API获取Notes数据进行采集
> 3. 修复图片采集重复问题
> 4. 其他功能性修复

### 2018.8.8

> 1. 更换Tumblr API
> 2. 代码部分重写
> 3. 去除原有自动采集功能
> 4. 初始化版本

## 相关链接

演示站点：[https://tumbex.xyz](https://tumbex.xyz)
作者主页：[https://www.lovekk.org](https://www.lovekk.org)