$(function() {
    $('input').keyup(function (e) {
        if (13 == e.keyCode) {
            $('button').trigger('click');
        }
    });
    $('#searchBtn').click(function() {
        if(isNull($('#search').val())) {
            $('#search').parent().addClass('has-error');
            $('.alert-danger').show();
            return false;
        }
        window.location.href = '/search/' + $('#search').val() + '.html';
    });
    $('#parseBtn').click(function() {
        if(isNull($('#blogName').val())) {
            $('#blogName').parent().addClass('has-error');
            $('.alert-danger').removeClass('hidden');
            $('#blogName').focus();
        } else {
            layer.open({
                type: 2,
                closeBtn: false,
                shade: 0.4,
                shadeClose: false,
                id: 'parseLoad',
                anim: 0,
                maxmin: false,
                resize: false,
                area: ['760px', '500px'],
                content: ['/parse/' + $('#blogName').val() + '.html', 'no'],
                end: function(){
                    if(null == blogId){
                        return layer.msg('解析失败，错误信息：' + msg);
                    }
                    window.location.href = '/' + blogId + '/video.html';
                }
            });
        }
    });
    $('.index .thumbnail').mouseover(function() {
        $(this).children('a').show();
    });
    $('.index .thumbnail').mouseout(function() {
        $(this).children('a').hide();
    });
    $('.videos .thumbnail a').click(function() {
        layer.open({
            type: 1,
            title: false,
            area: ['auto', 'auto'],
            shade: 0.8,
            closeBtn: 0,
            shadeClose: true,
            content: '<video controls="controls" poster="' + $(this).children('img').attr('src') + '" autoplay="autoplay" preload="auto" style="width:100%; max-width:100%;height:600px;"><source src="' + $(this).attr('data-href') + '" type="video/mp4"></video>'
        });
    });
    $('.photos .thumbnail a').click(function() {
        $.getJSON('/show/' + $(this).attr('data-id') + '.html', function(json) {
            layer.photos({
                photos: json,
                anim: 4,
                move: false
            });
        });
    });
});

function parse(size, pro) {
    $('#' + pro).attr('aria-valuenow', size);
    $('#' + pro).css('min-width', size + '%');
    $('#' + pro).css('width', size + '%');
    $('#' + pro).text(size + '%');
}

function parseClose(blogId, msg) {
    var index = parent.layer.getFrameIndex(window.name);
    parent.blogId = blogId;
    parent.msg = msg;
    parent.layer.close(index);
}

function isNull(str) {
    if(null == str || '' == str) {
        return true;
    }
    return false;
}