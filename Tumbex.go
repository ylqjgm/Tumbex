package main

import (
	_ "git.wskehao.com/ylqjgm/Tumbex/routers"
	"github.com/astaxie/beego"
	"git.wskehao.com/ylqjgm/Tumbex/controllers"
	"git.wskehao.com/ylqjgm/Tumbex/common"
				)

func main() {
	// 自定义错误信息
	beego.ErrorController(&controllers.ErrorController{})
	// 添加加载时间函数
	beego.AddFuncMap("loadtimes", common.LoadTimes)
	// 添加内存占用函数
	beego.AddFuncMap("memoryused", common.MemoryUsed)
	// 添加磁盘占用函数
	beego.AddFuncMap("diskused", common.DiskUsed)
	// 添加时间转换函数
	beego.AddFuncMap("duration", common.Duration)
	// 添加略缩图函数
	beego.AddFuncMap("thumbnail", common.Thumbnail)
	// 添加图片数量函数
	beego.AddFuncMap("photos", common.Photos)
	// 程序执行
	beego.Run()
}