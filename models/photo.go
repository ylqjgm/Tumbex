package models

import "gopkg.in/mgo.v2/bson"

// 图片信息结构体
type Photo struct {
	// 图片编号
	Id_ bson.ObjectId `bson:"_id"`
	// 文章编号
	PostId int64 `bson:"post_id"`
	// 图片地址
	Url string `bson:"url"`
}

// 获取图片列表
func GetPhotos(postId int64) (data []Photo, err error) {
	// 获取连接
	db := DBClone()
	// 使用后关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("photo")
	// 获取数据
	err = c.Find(bson.M{"post_id": postId}).All(&data)
	return
}

// 写入图片
func (p *Photo) Create() error {
	// 获取连接
	db := DBClone()
	// 使用后关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("photo")
	// 看看是否有了
	h, _ := c.Find(bson.M{"post_id": p.PostId, "url": p.Url}).Count()
	// 有就返回
	if h > 0 {
		return nil
	}
	return c.Insert(bson.M{"post_id": p.PostId, "url": p.Url})
}

// 获取图片统计
func GetPhotoCount() int {
	// 获取连接对象
	db := DBClone()
	// 关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("photo")
	// 获取统计
	nums, err := c.Count()
	// 如果有错
	if err != nil {
		return 0
	}
	return nums
}
