package models

import (
	"gopkg.in/mgo.v2/bson"
)

// 关系信息结构体
type Relation struct {
	// 博客编号
	BlogId bson.ObjectId `bson:"blog_id"`
	// 标签编号
	TagId bson.ObjectId `bson:"tag_id"`
}

// 关系检查
func GetRelation(blogId bson.ObjectId, tagId bson.ObjectId) bool {
	// 获取连接
	db := DBClone()
	// 使用后关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("relation")
	// 获取数据
	cnt, err := c.Find(bson.M{"blog_id": blogId, "tag_id": tagId}).Count()
	// 错误
	if err != nil || cnt == 0 {
		return false
	}
	return true
}

// 根据博客编号获取对应关系列表
func GetRelationByBlogId(blogId bson.ObjectId) (data []Relation, err error) {
	// 获取连接
	db := DBClone()
	// 使用后关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("relation")
	// 获取数据
	err = c.Find(bson.M{"blog_id": blogId}).All(&data)
	return
}

// 根据标签编号获取对应关系列表
func GetRelationByTagId(tagId bson.ObjectId) (data []Relation, err error) {
	// 获取连接
	db := DBClone()
	// 使用后关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("relation")
	// 获取数据
	err = c.Find(bson.M{"tag_id": tagId}).All(&data)
	return
}

// 创建对应关系
func (r *Relation) Create() error {
	// 获取连接
	db := DBClone()
	// 使用后关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("relation")
	// 看看是否有了
	h, _ := c.Find(bson.M{"blog_id": r.BlogId, "tag_id": r.TagId}).Count()
	// 有就返回
	if h > 0 {
		return nil
	}
	// 写入数据
	return c.Insert(bson.M{"blog_id": r.BlogId, "tag_id": r.TagId})
}
