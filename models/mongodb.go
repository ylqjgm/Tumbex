package models

import (
	"gopkg.in/mgo.v2"
	"time"
)

// 数据库连接对象
var db *mgo.Session

// 初始化数据库
func init() {
	// 连接数据库
	session, err := mgo.DialWithTimeout("", 10*time.Second)
	// 是否出错
	if nil != err {
		panic(err)
	}
	// 赋值给连接对象
	db = session
	// 设置模式
	db.SetMode(mgo.Monotonic, true)
	// 设置连接池
	db.SetPoolLimit(300)
	// 博客表索引
	blogIndex := mgo.Index{
		Key:        []string{"name"}, // 索引字段
		Unique:     true,             // 唯一索引
		DropDups:   false,            // 索引重复时是否替换旧文档, Unique开启时失效
		Background: true,             // 后台创建索引
	}
	// 创建索引
	db.DB("tumbex").C("blog").EnsureIndex(blogIndex)
	// 文章集索引
	postIndex := mgo.Index{
		Key:        []string{"blog_id", "post_type"},
		Unique:     false,
		DropDups:   true,
		Background: true,
	}
	// 创建索引
	db.DB("tumbex").C("post").EnsureIndex(postIndex)
	// 文章集唯一索引
	postUnique := mgo.Index{
		Key:        []string{"post_id"},
		Unique:     true,
		DropDups:   false,
		Background: true,
	}
	// 创建索引
	db.DB("tumbex").C("post").EnsureIndex(postUnique)
	// 视频集索引
	videoIndex := mgo.Index{
		Key:        []string{"post_id"},
		Unique:     true,
		DropDups:   false,
		Background: true,
	}
	// 创建索引
	db.DB("tumbex").C("video").EnsureIndex(videoIndex)
	// 图片集索引
	photoIndex := mgo.Index{
		Key:        []string{"post_id"},
		Unique:     false,
		DropDups:   true,
		Background: true,
	}
	// 创建索引
	db.DB("tumbex").C("photo").EnsureIndex(photoIndex)
	// 标签集索引
	tagIndex := mgo.Index{
		Key:        []string{"name"},
		Unique:     true,
		DropDups:   false,
		Background: true,
	}
	// 创建索引
	db.DB("tumbex").C("tag").EnsureIndex(tagIndex)
	// 关系集索引
	relationIndex := mgo.Index{
		Key:        []string{"blog_id", "tag_id"},
		Unique:     true,
		DropDups:   false,
		Background: true,
	}
	// 创建索引
	db.DB("tumbex").C("relation").EnsureIndex(relationIndex)
}

// 获取数据库连接
func DBClone() *mgo.Session {
	return db.Clone()
}
