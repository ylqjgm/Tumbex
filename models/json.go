package models

type JsonData struct {
	Title string      `json:"title"`
	Id    string      `json:"id"`
	Start int         `json:"start"`
	Data  []PhotoData `json:"data"`
}

type PhotoData struct {
	Alt   int    `json:"alt"`
	Pid   int    `json:"pid"`
	Src   string `json:"src"`
	Thumb string `json:"thumb"`
}
