package models

import "gopkg.in/mgo.v2/bson"

// 视频信息结构体
type Video struct {
	// 文章编号
	PostId int64 `bson:"post_id"`
	// 视频地址
	Url string `bson:"url"`
	// 封面地址
	Thumbnail string `bson:"thumbnail"`
	// 视频时长
	Duration int `bson:"duration"`
}

// 获取视频
func GetVideo(postId int64) (data Video, err error) {
	// 获取连接
	db := DBClone()
	// 使用后关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("video")
	// 获取数据
	err = c.Find(bson.M{"post_id": postId}).One(&data)
	return
}

// 写入视频
func (v *Video) Create() error {
	// 获取连接
	db := DBClone()
	// 使用后关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("video")
	// 看看是否有了
	h, _ := c.Find(bson.M{"post_id": v.PostId}).Count()
	// 有就返回
	if h > 0 {
		return nil
	}
	// 写入数据
	return c.Insert(bson.M{"post_id": v.PostId, "url": v.Url, "thumbnail": v.Thumbnail, "duration": v.Duration})
}

// 获取视频统计
func GetVideoCount() int {
	// 获取连接对象
	db := DBClone()
	// 关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("video")
	// 获取统计
	nums, err := c.Count()
	// 如果有错
	if err != nil {
		return 0
	}
	return nums
}
