package models

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

// 文章信息结构体
type Post struct {
	// 文章编号
	PostId int64 `bson:"post_id"`
	// 所属博客编号
	BlogId bson.ObjectId `bson:"blog_id"`
	// 文章类型
	PostType bool `bson:"post_type"`
	// 文章地址
	PostUrl string `bson:"post_url"`
	// 响应统计
	NoteCount int64 `bson:"note_count"`
	// 发布日期
	PostDate time.Time `bson:"post_date"`
	// 视频信息
	Video Video
	// 图片列表
	Photos []Photo
}

// 获取文章
func GetPost(postId int64) (data Post, err error) {
	// 获取连接
	db := DBClone()
	// 使用后关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("post")
	// 获取数据
	err = c.Find(bson.M{"post_id": postId}).One(&data)
	// 是否出错
	if nil != err {
		return
	}
	// 判断文章类型
	if data.PostType {
		// 获取图片列表
		data.Photos, _ = GetPhotos(data.PostId)
	} else {
		// 获取视频信息
		data.Video, _ = GetVideo(data.PostId)
	}
	return
}

// 获取文章列表
func GetPosts(blogId bson.ObjectId, postType bool, offset int) (data []Post, err error) {
	// 获取连接
	db := DBClone()
	// 使用后关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("post")
	// 获取数据
	err = c.Find(bson.M{"blog_id": blogId, "post_type": postType}).Skip(offset).Limit(20).Sort("-post_date").All(&data)
	return
}

// 通过博客编号获取文章统计
func GetPostCount(blogId bson.ObjectId, postType bool) int {
	// 获取连接
	db := DBClone()
	// 使用后关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("post")
	// 定义存储变量
	var ctn int
	// 没有传入文章类型则默认获取所有
	ctn, _ = c.Find(bson.M{"blog_id": blogId, "post_type": postType}).Count()
	return ctn
}

// 获取博客所有文章统计
func GetPostCounts(blogId bson.ObjectId) int {
	// 获取连接
	db := DBClone()
	// 使用后关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("post")
	// 定义存储变量
	var ctn int
	// 没有传入文章类型则默认获取所有
	ctn, _ = c.Find(bson.M{"blog_id": blogId}).Count()
	return ctn
}

// 写入文章
func (p *Post) Create() error {
	// 获取连接
	db := DBClone()
	// 使用后关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("post")
	// 先查看是否存在了
	err := c.Find(bson.M{"post_id": p.PostId}).One(p)
	// 有就返回
	if nil == err {
		return nil
	}
	// 写入数据
	return c.Insert(bson.M{"post_id": p.PostId, "blog_id": p.BlogId, "post_type": p.PostType, "post_url": p.PostUrl, "note_count": p.NoteCount, "post_date": p.PostDate})
}
