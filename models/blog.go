package models

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

// 博客信息结构体
type Blog struct {
	// 博客编号
	Id_ bson.ObjectId `bson:"_id"`
	// 博客标识
	Name string `bson:"name"`
	// 博客标题
	Title string `bson:"title"`
	// 博客描述
	Description string `bson:"description"`
	// 博客地址
	Url string `bson:"url"`
	// 博客写入时间
	CreateTime time.Time `bson:"create_time"`
}

// 通过标识获取博客信息
func GetBlogByName(name string) (data Blog, err error) {
	// 获取连接对象
	db := DBClone()
	// 使用后关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("blog")
	// 查询数据
	err = c.Find(bson.M{"name": name}).One(&data)
	return
}

// 通过编号获取博客信息
func GetBlogById(id bson.ObjectId) (data Blog, err error) {
	// 获取连接对象
	db := DBClone()
	// 使用后关闭连接
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("blog")
	// 查询数据
	err = c.Find(bson.M{"_id": id}).One(&data)
	if err != nil {
		panic(err)
	}
	return
}

// 分页获取博客列表
func GetBlogsByPage(page, limit int) (data []Blog) {
	// 获取连接对象
	db := DBClone()
	// 使用后关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("blog")
	// 获取数据
	err := c.Find(nil).Skip((page - 1) * limit).Sort("-_id").Limit(limit).All(&data)
	// 有错则返回空
	if err != nil {
		return []Blog{}
	}
	return
}

// 根据指定标签获取博客列表
func GetBlogsByTag(tagName string, page, limit int) (data []Blog, nums int, err error) {
	// 定义标签模型列表
	var tags []Tag
	// 获取标签列表
	tags, err = GetTags(tagName)
	// 如果没获取到
	if err != nil {
		return
	}
	// 获取连接对象
	db := DBClone()
	// 使用后关闭
	defer db.Close()
	// 选择集合
	r := db.DB("tumbex").C("relation")
	// 定义一个标签ID集合
	var tids []bson.ObjectId
	// 循环标签列表
	for _, tag := range tags {
		// 将编号赋值到集合中
		tids = append(tids, tag.Id_)
	}
	// 定义关系模型
	var relations []Relation
	// 查询所有对应关系
	err = r.Find(bson.M{"tag_id": bson.M{"$in": tids}}).All(&relations)
	// 如果错误
	if err != nil {
		return
	}
	// 定义一个ID集合
	var rids []bson.ObjectId
	// 循环获取到的对应关系
	for _, relation := range relations {
		// 将博客编号赋值到id集合中
		rids = append(rids, relation.BlogId)
	}
	// 重新选择数据集
	b := db.DB("tumbex").C("blog")
	// 获取统计数据
	nums, _ = b.Find(bson.M{"_id": bson.M{"$in": rids}}).Count()
	// 查询博客列表
	err = b.Find(bson.M{"_id": bson.M{"$in": rids}}).Skip((page - 1) * limit).Sort("-create_time").Limit(limit).All(&data)
	return
}

// 写入博客信息
func (b *Blog) Create() error {
	// 获取连接对象
	db := DBClone()
	// 使用后关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("blog")
	// 看看是否有了
	err := c.Find(bson.M{"name": b.Name}).One(b)
	// 有就返回
	if nil == err {
		return nil
	}
	// 是否有id了
	if !b.Id_.Valid() {
		// 生成一个ID
		b.Id_ = bson.NewObjectId()
	}
	// 写入数据
	return c.Insert(bson.M{"_id": b.Id_, "name": b.Name, "title": b.Title, "description": b.Description, "url": b.Url, "create_time": bson.Now()})
}

// 删除博客
func (b *Blog) Delete() error {
	// 获取连接对象
	db := DBClone()
	// 使用后关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("blog")
	return c.Remove(bson.M{"_id": b.Id_})
}

// 获取博客统计
func GetBlogCount() int {
	// 获取连接对象
	db := DBClone()
	// 关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("blog")
	// 获取统计
	nums, err := c.Count()
	// 如果有错
	if err != nil {
		return 0
	}
	return nums
}
