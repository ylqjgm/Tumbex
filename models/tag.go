package models

import "gopkg.in/mgo.v2/bson"

// 标签信息结构体
type Tag struct {
	// 标签编号
	Id_ bson.ObjectId `bson:"_id"`
	// 标签名称
	Name string `bson:"name"`
}

// 模糊查询标签列表
func GetTags(tagName string) (data []Tag, err error) {
	// 获取连接
	db := DBClone()
	// 使用后关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("tag")
	// 获取数据
	err = c.Find(bson.M{"name": bson.M{"$regex": tagName, "$options": "$i"}}).All(&data)
	return
}

// 获取标签数据
func GetTag(tagName string) (data Tag, err error) {
	// 获取连接
	db := DBClone()
	// 使用后关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("tag")
	// 查询数据
	err = c.Find(bson.M{"name": tagName}).One(&data)
	return
}

// 写入标签
func (t *Tag) Create() error {
	// 获取连接
	db := DBClone()
	// 使用后关闭
	defer db.Close()
	// 选择集合
	c := db.DB("tumbex").C("tag")
	// 看看是否有了
	err := c.Find(bson.M{"name": t.Name}).One(t)
	// 有就返回
	if nil == err {
		return nil
	}
	// 是否有id了
	if !t.Id_.Valid() {
		// 生成一个ID
		t.Id_ = bson.NewObjectId()
	}
	// 写入数据
	return c.Insert(bson.M{"_id": t.Id_, "name": t.Name})
}
