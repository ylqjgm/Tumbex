package controllers

import (
	"git.wskehao.com/ylqjgm/Tumbex/common"
	"gopkg.in/mgo.v2/bson"
	"github.com/astaxie/beego/utils/pagination"
	"math"
)

type VideoController struct {
	BaseController
}

// 视频列表显示方法
func (c *VideoController) List() {
	// 获取博客编号
	id := c.Ctx.Input.Param(":id")
	// 获取不到
	if "" == id {
		c.Abort("404")
	}
	// 获取当前第几页
	p, err := c.GetInt("p")
	// 获取失败
	if nil != err {
		// 默认第一页
		p = 1
	}
	// 获取博客信息
	blog, err := common.GetBlogByDb(bson.ObjectIdHex(id))
	// 是否出错
	if nil != err {
		c.Abort("404")
	}
    // 如果是第一页
    if 1 == p {
    	// 获取博客信息
    	_, vt, pt, _ := common.GetBlogByApi(blog.Name)
    	// 如果视频文章数量不对
    	if !common.CheckPost(blog, false, vt) {
    		// 计算总页数
    		pages := int(math.Ceil(float64(vt) / 20))
    		// 循环页面
    		for i := 1; i <= pages; i++ {
    			// 获取并写入数据库
    			common.GetAndWritePosts(blog, "video", (i-1)*20, 20)
    		}
    	}
    	// 如果图片文章数量不对
    	if !common.CheckPost(blog, true, pt) {
    		// 计算总页数
    		pages := int(math.Ceil(float64(vt) / 20))
    		// 循环页面
    		for i := 1; i <= pages; i++ {
    			// 获取并写入数据库
    			common.GetAndWritePosts(blog, "photo", (i-1)*20, 20)
    		}
    	}
    }
	// 获取视频列表
	data, err := common.GetVideos(blog.Id_, p)
	// 是否出错
	if nil != err {
		c.Abort("404")
	}
	// 获取视频总量
	totals := common.GetVideoCount(blog.Id_)
	// 分页处理
	page := pagination.SetPaginator(c.Ctx, 20, int64(totals))
	// 设置博客信息
	c.Data["Blog"] = blog
	// 设置视频统计
	c.Data["Totals"] = totals
	// 设置视频列表
	c.Data["List"] = data
	// 设置分页
	c.Data["paginator"] = page
	// 当前位置为解析
	c.Data["Current"] = "parse"
	// 显示模板
	c.TplName = "video.html"
}
