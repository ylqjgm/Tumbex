package controllers

import (
	"git.wskehao.com/ylqjgm/Tumbex/common"
	"github.com/astaxie/beego/utils/pagination"
)

type SearchController struct {
	BaseController
}

// 搜索页面显示方法
func (c *SearchController) Index() {
	// 当前位置为搜索
	c.Data["Current"] = "search"
	// 设置统计数据
	c.Data["Count"] = common.Count()
	// 显示模板
	c.TplName = "search.html"
}

// 搜索结果显示方法
func (c *SearchController) Result() {
	// 获取搜索关键字
	key := c.Ctx.Input.Param(":key")
	// 如果关键字为空
	// 如果关键字为空
	if key == "" {
		// 抛出404
		c.Abort("404")
	}
	// 获取当前页
	p, err := c.GetInt("p")
	// 错误则指定为1
	if err != nil {
		p = 1
	}
	// 获取搜索结果
	data, totals, _ := common.SearchByTag(key, p, 24)
	// 分页
	page := pagination.SetPaginator(c.Ctx, 24, int64(totals))
	// 设置关键字
	c.Data["Key"] = key
	// 设置数据总量
	c.Data["Totals"] = totals
	// 设置数据
	c.Data["List"] = data
	// 设置分页
	c.Data["paginator"] = page
	// 设置当前位置
	c.Data["Current"] = "search"
	// 显示模板
	c.TplName = "result.html"
}
