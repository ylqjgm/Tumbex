package controllers

import "github.com/astaxie/beego"

// Controller结构体
type ErrorController struct {
	beego.Controller
}

// 404错误页
func (c *ErrorController) Error404() {
	c.TplName = "public/error.html"
}

// 500错误页
func (c *ErrorController) Error500() {
	c.TplName = "public/error.html"
}
