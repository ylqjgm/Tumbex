package controllers

import (
	"git.wskehao.com/ylqjgm/Tumbex/common"
	"github.com/astaxie/beego/utils/pagination"
	"git.wskehao.com/ylqjgm/Tumbex/models"
)

type IndexController struct {
	BaseController
}

// 首页显示方法
func (c *IndexController) Index() {
	// 当前位置为首页
	c.Data["Current"] = "home"
	// 获取统计数据
	count := common.Count()
	// 设置统计数据
	c.Data["Count"] = count
	// 设置分页
	page := pagination.SetPaginator(c.Ctx, 24, int64(count["Blog"]))
	// 设置分页数据
	c.Data["paginator"] = page
	// 获取当前页
	p, err := c.GetInt("p")
	// 如果获取不到
	if nil != err {
		// 设置默认为1
		p = 1
	}
	// 获取分页数据
	c.Data["List"] = models.GetBlogsByPage(p, 24)
	// 显示模板
	c.TplName = "index.html"
}
