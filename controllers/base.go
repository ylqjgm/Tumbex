package controllers

import (
	"github.com/astaxie/beego"
	"time"
	"runtime"
)

// 基础控制器
type BaseController struct {
	beego.Controller
}

// 载入前方法
func (c *BaseController) Prepare() {
	// 设置Go版本
	c.Data["GoVersion"] = runtime.Version()
	// 设置开始运行时间
	c.Data["StartTime"] = time.Now()
}
