package controllers

import (
	"git.wskehao.com/ylqjgm/Tumbex/common"
	"fmt"
	"strconv"
	"math"
	"strings"
)

type ParseController struct {
	BaseController
}

// 解析页面显示方法
func (c *ParseController) Index() {
	// 当前位置为解析
	c.Data["Current"] = "parse"
	// 显示模板
	c.TplName = "parse.html"
}

// 解析操作方法
func (c *ParseController) Parse() {
	// 输出基础HTML
	fmt.Fprint(c.Ctx.ResponseWriter, `<!DOCTYPE html><html lang="zh-CN"><head><meta charset="UTF-8"><link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css"></head>`)
	fmt.Fprint(c.Ctx.ResponseWriter, `<body><div class="container"><div class="row"><div class="col-md-6 col-md-offset-3">`)
	fmt.Fprint(c.Ctx.ResponseWriter, `<div class="jumbotron"><h1></h1><br>`)
	fmt.Fprint(c.Ctx.ResponseWriter, `<p><button class="btn btn-success" type="button">共有视频文章 <span id="vt" class="badge"></span></button>&nbsp;&nbsp;<button class="btn btn-success" type="button">共有图片文章 <span id="pt" class="badge"></span></button></p><br>`)
	fmt.Fprint(c.Ctx.ResponseWriter, `<p class="text-center"><div id="vm" class="alert alert-success" role="alert">等待解析...</div></p>`)
	fmt.Fprint(c.Ctx.ResponseWriter, `<p><div class="progress"><div id="vp" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="min-width:0%;width:0%;">0%</div></div></p>`)
	fmt.Fprint(c.Ctx.ResponseWriter, `<p class="text-center"><div id="pm" class="alert alert-success" role="alert">等待解析...</div></p>`)
	fmt.Fprint(c.Ctx.ResponseWriter, `<p><div class="progress"><div id="pp" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="min-width:0%;width:0%;">0%</div></div></p>`)
	fmt.Fprint(c.Ctx.ResponseWriter, `<script src="//cdn.bootcss.com/jquery/3.3.0/jquery.min.js"></script><script src="//cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script><script src="//cdn.bootcss.com/layer/3.1.0/layer.js"></script><script src="/static/js/common.js"></script></body></html>`)
	// 获取博客标识
	name := c.Ctx.Input.Param(":name")
	// 去除空白
	name = strings.TrimSpace(name)
	// 获取不到
	if "" == name {
		// 关闭Frame
		fmt.Fprint(c.Ctx.ResponseWriter, `<script>parseClose(null,'未传入博客标识');</script>`)
		// 输出
		c.Ctx.WriteString("")
		return
	}
	// 获取博客信息
	b, vt, pt, err := common.GetBlogByApi(name)
	// 是否出错
	if nil != err {
		fmt.Fprint(c.Ctx.ResponseWriter, fmt.Sprintf(`<script>parseClose(null, '资源请求错误，错误代码：%s');</script>`, err.Error()))
		c.Ctx.WriteString("")
		return
	}
	// 显示标题
	fmt.Fprint(c.Ctx.ResponseWriter, `<script>$('h1').text('正在解析博客标识为 `+name+` 的文章数据...');</script>`)
	// 显示视频文章数量
	fmt.Fprint(c.Ctx.ResponseWriter, `<script>$('#vt').text('`+strconv.Itoa(vt)+`');</script>`)
	// 显示图片文章数量
	fmt.Fprint(c.Ctx.ResponseWriter, `<script>$('#pt').text('`+strconv.Itoa(pt)+`');</script>`)
	// 如果视频文章数量不对
	if !common.CheckPost(b, false, vt) {
		// 显示视频采集提示
		fmt.Fprint(c.Ctx.ResponseWriter, `<script>$('#vm').text('正在解析视频文章...');</script>`)
		// 计算总页数
		pages := int(math.Ceil(float64(vt) / 20))
		// 计算每页代表的进度条数
		size := float64(100) / float64(pages);
		// 循环页面
		for i := 1; i <= pages; i++ {
			// 获取并写入数据库
			common.GetAndWritePosts(b, "video", (i-1)*20, 20)
			// 设置进度条
			fmt.Fprint(c.Ctx.ResponseWriter, `<script>parse('`+fmt.Sprintf("%.2f", size*float64(i))+`', 'vp');</script>`)
			// 刷新缓冲
			c.Ctx.ResponseWriter.Flush()
		}
	}
	// 如果图片文章数量不对
	if !common.CheckPost(b, true, pt) {
		// 显示图片采集提示
		fmt.Fprint(c.Ctx.ResponseWriter, `<script>$('#pm').text('正在解析图片文章...');</script>`)
		// 计算总页数
		pages := int(math.Ceil(float64(vt) / 20))
		// 计算每页代表的进度条数
		size := float64(100) / float64(pages);
		// 循环页面
		for i := 1; i <= pages; i++ {
			// 获取并写入数据库
			common.GetAndWritePosts(b, "photo", (i-1)*20, 20)
			// 设置进度条
			fmt.Fprint(c.Ctx.ResponseWriter, `<script>parse('`+fmt.Sprintf("%.2f", size*float64(i))+`', 'pp');</script>`)
			// 刷新缓冲
			c.Ctx.ResponseWriter.Flush()
		}
	}
	// 输出关闭
	fmt.Fprint(c.Ctx.ResponseWriter, `<script>parseClose('`+b.Id_.Hex()+`');</script>`)
	// 输出
	c.Ctx.WriteString("")
	return
}
