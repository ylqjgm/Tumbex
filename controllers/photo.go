package controllers

import (
	"git.wskehao.com/ylqjgm/Tumbex/common"
	"gopkg.in/mgo.v2/bson"
	"github.com/astaxie/beego/utils/pagination"
	"strconv"
)

type PhotoController struct {
	BaseController
}

func (c *PhotoController) List() {
	// 获取博客编号
	id := c.Ctx.Input.Param(":id")
	// 获取不到
	if "" == id {
		c.Abort("404")
	}
	// 获取当前第几页
	p, err := c.GetInt("p")
	// 获取失败
	if nil != err {
		// 默认第一页
		p = 1
	}
	// 获取博客信息
	blog, err := common.GetBlogByDb(bson.ObjectIdHex(id))
	// 是否出错
	if nil != err {
		c.Abort("404")
	}
	// 获取图片列表
	data, err := common.GetPhotos(blog.Id_, p)
	// 是否出错
	if nil != err {
		c.Abort("404")
	}
	// 获取图片总量
	totals := common.GetPhotoCount(blog.Id_)
	// 分页处理
	page := pagination.SetPaginator(c.Ctx, 20, int64(totals))
	// 设置博客信息
	c.Data["Blog"] = blog
	// 设置图片统计
	c.Data["Totals"] = totals
	// 设置图片列表
	c.Data["List"] = data
	// 设置分页
	c.Data["paginator"] = page
	// 当前位置为解析
	c.Data["Current"] = "parse"
	// 显示模板
	c.TplName = "photo.html"
}

func (c *PhotoController) Show() {
	// 获取文章编号
	id := c.Ctx.Input.Param(":id")
	// 没有获取到则报错
	if id == "" {
		c.Data["json"] = map[string]string{"status": "1", "msg": "没有获取到ID"}
		c.ServeJSON()
	}
	// 转换ID为int64格式
	postId, err := strconv.ParseInt(id, 10, 64)
	// 如果出错则报错
	if err != nil {
		c.Data["json"] = map[string]string{"status": "1", "msg": "没有获取到ID"}
		c.ServeJSON()
	}
	// 获取图片列表Json
	json, err := common.GetPhotosJson(postId)
	// 如果出错
	if nil != err {
		c.Data["json"] = map[string]string{"status": "1", "msg": "图片获取失败"}
		c.ServeJSON()
	}
	// 设置标题
	json.Title = id
	// 设置编号
	json.Id = id
	// 设置起始位置
	json.Start = 0
	// 设置JSON数据
	c.Data["json"] = json
	// 输出
	c.ServeJSON()
}
