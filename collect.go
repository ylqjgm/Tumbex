package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"net/url"
	"strings"
	"time"

	"git.wskehao.com/ylqjgm/Tumbex/common"
	"git.wskehao.com/ylqjgm/Tumbex/models"
	"github.com/PuerkitoBio/goquery"
	"github.com/pkg/errors"
	"github.com/tidwall/gjson"
)

type TangBure struct {
	*goquery.Document
}

// 采集
func main() {
	// 计数
	loop := 1
	// 死循环
	for {
		// 输出记录
		log.Printf("开始采集第 %d 次...\n", loop)
		// 获取博客数量
		totals := models.GetBlogCount()
		// 输出记录
		log.Printf("获取到 %d 条博客记录...\n", totals)
		// 计算分页
		pages := int(math.Ceil(float64(totals) / 20))
		// 输出记录
		log.Printf("共分为 %d 页...\n", pages)
		// 循环获取博客列表
		for i := 1; i <= pages; i++ {
			// 输出记录
			log.Printf("开始采集第 %d 页...\n", i)
			// 获取博客列表
			blogs := models.GetBlogsByPage(i, 20)
			// 循环博客列表
			for _, blog := range blogs {
				// 获取文章统计
				totals := models.GetPostCounts(blog.Id_)
				// 如果为空
				if 0 == totals {
					// 输出记录
					log.Printf("标识为 %s 的博客没有文章, 将删除数据...\n", blog.Name)
					// 删除博客
					blog.Delete()
					// 跳过本次
					continue
				}
				// 输出记录
				log.Printf("开始采集博客标识为 %s 的响应数据...\n", blog.Name)
				// 执行响应采集
				GetNotes(blog.Name)
				// 输出记录
				log.Printf("博客标识为 %s 的响应数据采集完成...\n", blog.Name)
				// 暂停10分钟
				time.Sleep(time.Minute * 10)
			}
			// 输出记录
			log.Printf("第 %d 页采集结束...\n", i)
			// 暂停半小时
			time.Sleep(time.Minute * 30)
		}
		// 输出记录
		log.Printf("第 %d 采集结束, 暂停一小时...\n", loop)
		// 计数自增
		loop++
		// 暂停两小时
		time.Sleep(time.Hour * 2)
	}
}

// 采集Notes并入库博客信息
func GetNotes(name string) {
	// 是否传入了标识
	if "" == name {
		return
	}
	// 组合URL
	uri := fmt.Sprintf("https://api.tumblr.com/v2/blog/%s.tumblr.com/posts?api_key=fuiKNFp9vQFvjLNvx4sUwti4Yb5yGutBN4Xh10LXZhhRKjWlV4&notes_info=true&offset=0&limit=1000000000", name)
	// 设置Client
	client := &http.Client{
		// 设置跳转处理
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			// 返回跳转地址
			return errors.New(req.URL.String())
		},
	}
	// 发起Get请求
	res, err := client.Get(uri)
	// 若有错
	if nil != err {
		// 是否为url错误
		if e, ok := err.(*url.Error); ok && e.Err != nil {
			// 替换跳转链接，把http换为https
			uri := strings.Replace(e.URL, "http://", "https://", -1)
			// 重新发起Get请求
			res, err = client.Get(uri)
			// 还有错误
			if nil != err {
				// 输出记录
				log.Printf("标识为 %s 的博客请求API失败, 失败信息：%s...\n", name, err.Error())
				return
			}
		} else {
			// 输出记录
			log.Printf("标识为 %s 的博客请求API失败, 失败信息：%s...\n", name, err.Error())
			return
		}
	}
	// 关闭连接
	defer res.Body.Close()
	// 检查请求状态
	if 200 != res.StatusCode {
		// 输出记录
		log.Printf("标识为 %s 的博客请求API失败, 返回失败代码：%d...\n", name, res.StatusCode)
		return
	}
	// 转换为字符串
	r, err := ioutil.ReadAll(res.Body)
	// 是否成功
	if nil != err {
		// 输出记录
		log.Printf("标识为 %s 的博客请求API失败, 失败信息：%s...\n", name, err.Error())
		return
	}
	// 解析Json
	json := gjson.Parse(string(r))
	// 检查是否正确
	status := json.Get("meta.status")
	// 是否正确
	if "200" != status.String() {
		// 输出记录
		log.Printf("标识为 %s 的博客请求API失败, 失败信息：%s...\n", name, status.String())
		return
	}
	// 循环posts
	json.Get("response.posts").ForEach(func(key, post gjson.Result) bool {
		// 循环notes
		post.Get("notes").ForEach(func(k, n gjson.Result) bool {
			// 获取标识
			bn := n.Get("blog_name")
			// 获取博客视频数据
			b, vt, err := common.GetBlogNotWrite(bn.String(), "video")
			// 是否出错
			if nil != err {
				// 输出记录
				log.Printf("标识为 %s 的博客视频文章获取失败, 失败信息：%s...\n", bn.String(), err.Error())
				return false
			}
			// 获取博客图片信息
			_, pt, err := common.GetBlogNotWrite(bn.String(), "photo")
			// 是否出错
			if nil != err {
				// 输出记录
				log.Printf("标识为 %s 的博客图片文章获取失败, 失败信息：%s...\n", bn.String(), err.Error())
				return false
			}
			// 如果没有文章
			if 0 == vt && 0 == pt {
				// 输出记录
				log.Printf("跳过标识为 %s 的博客采集, 跳过原因：无文章数据...\n", bn.String())
				// 返回
				return false
			}
			// 写入博客信息
			err = b.Create()
			// 是否出错
			if nil != err {
				// 输出记录
				log.Printf("跳过标识为 %s 的博客采集, 失败原因：博客写入失败，错误信息：%s...\n", b.Name, err.Error())
				return false
			}
			// 如果视频文章数量不对
			if !common.CheckPost(b, false, vt) {
				// 输出记录
				log.Printf("开始采集标识为 %s 的视频文章, 总计 %d 篇...\n", b.Name, vt)
				// 计算总页数
				pages := int(math.Ceil(float64(vt) / 20))
				// 循环页面
				for i := 1; i <= pages; i++ {
					// 获取并写入数据库
					common.GetAndWritePosts(b, "video", (i-1)*20, 20)
				}
				// 输出记录
				log.Printf("标识为 %s 的博客视频文章采集结束...\n", b.Name)
			}
			// 如果图片文章数量不对
			if !common.CheckPost(b, true, pt) {
				// 输出记录
				log.Printf("开始采集标识为 %s 的图片文章, 总计 %d 篇...\n", b.Name, pt)
				// 计算总页数
				pages := int(math.Ceil(float64(vt) / 20))
				// 循环页面
				for i := 1; i <= pages; i++ {
					// 获取并写入数据库
					common.GetAndWritePosts(b, "photo", (i-1)*20, 20)
				}
				// 输出记录
				log.Printf("标识为 %s 的博客图片文章采集结束...\n", b.Name)
			}
			return true
		})
		return true
	})
}

// 获取HTML
func GetHtml(uri string) (t *TangBure, err error) {
	// 发起Get请求
	res, err := http.Get(uri)
	// 若有错
	if nil != err {
		// 输出记录
		log.Printf("采集失败, 失败信息：%s...\n", err.Error())
		return
	}
	// 关闭连接
	defer res.Body.Close()
	// 检查请求状态
	if 200 != res.StatusCode {
		// 输出记录
		log.Printf("采集失败, 返回失败代码：%d...\n", res.StatusCode)
		return
	}
	// 获取doc对象
	doc, err := goquery.NewDocumentFromReader(res.Body)
	// 是否出错
	if nil != err {
		// 输出记录
		log.Printf("转换数据失败，失败信息：%s...\n", err.Error())
		return
	}
	return &TangBure{doc}, nil
}

// 采集数据
func GetBlog(name string) error {
	// 如果没有传入标识
	if "" == name {
		return errors.New("没有传入博客标识")
	}
	// 获取博客视频数据
	b, vt, err := common.GetBlogNotWrite(name, "video")
	// 是否出错
	if nil != err {
		return err
	}
	// 获取博客图片信息
	_, pt, err := common.GetBlogNotWrite(name, "photo")
	// 是否出错
	if nil != err {
		return err
	}
	// 如果没有文章
	if 0 == vt && 0 == pt {
		return errors.New("该博客没有文章数据")
	}
	// 写入博客信息
	err = b.Create()
	// 是否出错
	if nil != err {
		return err
	}
	// 如果视频文章数量不对
	if !common.CheckPost(b, false, vt) {
		// 输出记录
		log.Printf("开始采集标识为 %s 的视频文章, 总计 %d 篇...\n", b.Name, vt)
		// 计算总页数
		pages := int(math.Ceil(float64(vt) / 20))
		// 循环页面
		for i := 1; i <= pages; i++ {
			// 获取并写入数据库
			common.GetAndWritePosts(b, "video", (i-1)*20, 20)
		}
		// 输出记录
		log.Printf("标识为 %s 的博客视频文章采集结束...\n", b.Name)
	}
	// 如果图片文章数量不对
	if !common.CheckPost(b, true, pt) {
		// 输出记录
		log.Printf("开始采集标识为 %s 的图片文章, 总计 %d 篇...\n", b.Name, pt)
		// 计算总页数
		pages := int(math.Ceil(float64(vt) / 20))
		// 循环页面
		for i := 1; i <= pages; i++ {
			// 获取并写入数据库
			common.GetAndWritePosts(b, "photo", (i-1)*20, 20)
		}
		// 输出记录
		log.Printf("标识为 %s 的博客图片文章采集结束...\n", b.Name)
	}
	return nil
}
