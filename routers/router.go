package routers

import (
	"git.wskehao.com/ylqjgm/Tumbex/controllers"
	"github.com/astaxie/beego"
)

// 路由设置
func init() {
	// 首页路由
	beego.Router("/", &controllers.IndexController{}, "get:Index")
	// 搜索页路由
	beego.Router("/search.html", &controllers.SearchController{}, "get:Index")
	// 搜索结果路由
	beego.Router("/search/:key.html", &controllers.SearchController{}, "get:Result")
	// 解析页获取路由
	beego.Router("/parse/:name.html", &controllers.ParseController{}, "get:Parse")
	// 解析页路由
	beego.Router("/parse.html", &controllers.ParseController{}, "get:Index")
	// 视频列表显示路由
	beego.Router("/:id/video.html", &controllers.VideoController{}, "get:List")
	// 图片列表显示路由
	beego.Router("/:id/photo.html", &controllers.PhotoController{}, "get:List")
	// 图片相册显示路由
	beego.Router("/show/:id.html", &controllers.PhotoController{}, "get:Show")
}
