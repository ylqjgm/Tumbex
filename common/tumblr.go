package common

import (
	"fmt"
	"net/http"
	"github.com/pkg/errors"
	"github.com/PuerkitoBio/goquery"
	"net/url"
	"strings"
	"strconv"
	"git.wskehao.com/ylqjgm/Tumbex/models"
	"time"
	"regexp"
	"github.com/tidwall/gjson"
	"io/ioutil"
)

type Tumblr struct {
	gjson.Result
}

// 请求API
func GetApi(name string, t string, start, limit int) (tumblr *Tumblr, err error) {
	// 是否传入了标识
	if "" == name {
		return tumblr, errors.New("No Name")
	}
	// 是否传入文章类型
	if "" == t {
		// 设置文章类型为视频
		t = "video"
	}
	// 组合URL
	uri := fmt.Sprintf("https://%s.tumblr.com/api/read/json?type=%s&num=%d&start=%d", name, t, limit, start)
	// 设置Client
	client := &http.Client{
		// 设置跳转处理
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			// 返回跳转地址
			return errors.New(req.URL.String())
		},
	}
	// 发起Get请求
	res, err := client.Get(uri)
	// 若有错
	if nil != err {
		// 是否为url错误
		if e, ok := err.(*url.Error); ok && e.Err != nil {
			// 替换跳转链接，把http换为https
			uri := strings.Replace(e.URL, "http://", "https://", -1)
			// 重新发起Get请求
			res, err = client.Get(uri)
			// 还有错误
			if nil != err {
				return
			}
		} else {
			return
		}
	}
	// 关闭连接
	defer res.Body.Close()
	// 检查请求状态
	if 200 != res.StatusCode {
		return tumblr, errors.New(fmt.Sprintf("%d", res.StatusCode))
	}
	// 转换为字符串
	r, err := ioutil.ReadAll(res.Body)
	// 是否成功
	if nil != err {
		return
	}
	// 去掉非Json数据
	json := strings.Replace(string(r), "var tumblr_api_read = ", "", -1)
	// 返回数据
	return &Tumblr{gjson.Parse(json)}, nil
}

// 获取博客信息
func (t *Tumblr) GetBlog() models.Blog {
	// 获取博客标识
	name := t.Get("tumblelog.name")
	// 获取博客标题
	title := t.Get("tumblelog.title")
	// 获取博客描述
	description := t.Get("tumblelog.description")
	// 组合博客地址
	uri := fmt.Sprintf("https://%s.tumblr.com", name.String())
	// 返回
	return models.Blog{
		Name:        trimHtml(name.String()),
		Title:       trimHtml(title.String()),
		Description: trimHtml(description.String()),
		Url:         trimHtml(uri),
	}
}

// 获取文章数量
func (t *Tumblr) GetTotals() int {
	// 获取total属性
	totals := t.Get("posts-total")
	// 转换为数字
	total, err := strconv.Atoi(totals.String())
	// 是否出错
	if nil != err {
		return 0
	}
	return total
}

// 获取文章列表
func (t *Tumblr) GetPosts() (posts []models.Post, tags []models.Tag) {
	t.Get("posts").ForEach(func(i, s gjson.Result) bool {
		// 获取文章编号
		id := s.Get("id")
		// 转换为int64格式
		postId, err := strconv.ParseInt(id.String(), 10, 64)
		// 是否出错
		if nil != err {
			return false
		}
		// 获取文章地址
		postUrl := s.Get("url")
		// 获取文章类型
		postType := s.Get("type")
		// 文章类型变量
		var post_type bool
		// 定义视频对象
		var video models.Video
		// 定义图片列表对象
		var photos []models.Photo
		// 转换文章类型格式
		if "video" == postType.String() {
			// 设置为false
			post_type = false
			// 获取视频数据
			video = GetVideo(s, postId)
		} else if "photo" == postType.String() {
			// 设置为true
			post_type = true
			// 清空图片列表
			photos = GetPhoto(s, postId)
		} else {
			return false
		}
		// 获取响应统计
		note := s.Get("note-count")
		// 转换为int64
		noteCount, err := strconv.ParseInt(note.String(), 10, 64)
		// 是否出错
		if nil != err {
			noteCount = 0
		}
		// 获取发表时间戳
		unix := s.Get("unix-timestamp")
		// 转换时间戳为int64格式
		timestamp, err := strconv.ParseInt(unix.String(), 10, 64)
		// 是否转换错误
		if nil != err {
			timestamp = time.Now().Unix()
		}
		// 组合文章结构
		post := models.Post{
			PostId:    postId,
			PostType:  post_type,
			PostUrl:   postUrl.String(),
			NoteCount: noteCount,
			PostDate:  time.Unix(timestamp, 0),
			Video:     video,
			Photos:    photos,
		}
		// 加入文章列表
		posts = append(posts, post)
		// 循环标签
		s.Get("tags").ForEach(func(k, v gjson.Result) bool {
			// 标签对象
			tag := models.Tag{
				Name: v.String(),
			}
			// 加入列表
			tags = append(tags, tag)
			return true
		})
		return true
	})
	return
}

// 获取视频数据
func GetVideo(s gjson.Result, postId int64) models.Video {
	// 获取视频数据
	h := s.Get("video-player")
	// 转换视频内容为goquery对象
	v, _ := goquery.NewDocumentFromReader(strings.NewReader(h.String()))
	// 获取封面属性
	poster, _ := v.Find("video").Attr("poster")
	// 获取播放地址
	uri, _ := v.Find("source").Attr("src")
	// 获取data-crt-options属性
	options, _ := v.Find("video").Attr("data-crt-options")
	// 获取时长
	dur := gjson.Get(options, "duration")
	// 转换为int
	duration, err := strconv.Atoi(dur.String())
	// 是否出错
	if nil != err {
		duration = 0
	}
	// 组合视频数据
	return models.Video{
		PostId:    postId,
		Url:       uri,
		Thumbnail: poster,
		Duration:  duration,
	}
}

// 获取图片列表
func GetPhoto(s gjson.Result, postId int64) []models.Photo {
	// 定义图片列表
	var photos []models.Photo
	// 获取photos
	ps := s.Get("photos")
	// 是否存在
	if !ps.Exists() || 0 == len(ps.Array()) {
		// 直接获取
		uri := s.Get("photo-url-1280")
		// 加入图片列表
		photos = append(photos, models.Photo{
			PostId: postId,
			Url:    strings.TrimSpace(uri.String()),
		})
	} else {
		// 循环photos
		s.Get("photos").ForEach(func(k, v gjson.Result) bool {
			// 获取第一条
			uri := v.Get("photo-url-1280")
			// 加入图片列表
			photos = append(photos, models.Photo{
				PostId: postId,
				Url:    strings.TrimSpace(uri.String()),
			})
			return true
		})
	}
	return photos
}

// 去除HTML
func trimHtml(src string) string {
	// 大小写转换正则
	re, _ := regexp.Compile("\\<[\\S\\s]+?\\>")
	// 将HTML标签全部转换为小写
	src = re.ReplaceAllStringFunc(src, strings.ToLower)
	// 去除style正则
	re, _ = regexp.Compile("\\<style[\\S\\s]+?\\</style\\>")
	// 去除style
	src = re.ReplaceAllString(src, "")
	// 去除script正则
	re, _ = regexp.Compile("\\<script[\\S\\s]+?\\</script\\>")
	// 去除script
	src = re.ReplaceAllString(src, "")
	// 去除所有html正则
	re, _ = regexp.Compile("\\<[\\S\\s]+?\\>")
	// 去除html
	src = re.ReplaceAllString(src, "\n")
	// 去除连续的换行符正则
	re, _ = regexp.Compile("\\s{2,}")
	// 去除连续换行符
	src = re.ReplaceAllString(src, "\n")

	return strings.TrimSpace(src)
}
