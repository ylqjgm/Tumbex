package common

import (
	"time"
	"fmt"
	"github.com/shirou/gopsutil/mem"
	"math"
	"strings"
	"git.wskehao.com/ylqjgm/Tumbex/models"
	"github.com/shirou/gopsutil/disk"
)

// 加载时间计算
func LoadTimes(startTime time.Time) string {
	return fmt.Sprintf("%.5f", time.Now().Sub(startTime).Seconds())
}

// 硬盘使用计算
func DiskUsed() string {
	d, _ := disk.Usage("/")
	return fmt.Sprintf("%d", d.Used/1024/1024/1024)
}

// 计算内存占用
func MemoryUsed() string {
	// 获取内存详情
	v, _ := mem.VirtualMemory()
	// 返回内存占用，单位MB
	return fmt.Sprintf("%d", v.Used/1024/1024)
}

// 将秒数转换为时长
func Duration(duration int) string {
	// 转换为float64
	dur := float64(duration)
	// 默认时长
	result := "00:00:00"
	// 如果秒数大于0
	if duration > 0 {
		// 计算小时
		hour := math.Floor(dur / 3600)
		// 计算分钟
		minute := math.Floor((dur - 3600*hour) / 60)
		// 计算秒
		second := math.Mod(math.Floor((dur-3600*hour)-60*minute), 60)
		// 组合时长
		result = fmt.Sprintf("%02.f:%02.f:%02.f", hour, minute, second)
	}
	return result
}

// 获取图片略缩图
func Thumbnail(p []models.Photo) string {
	// 获取第一张图片
	img := p[0].Url
	// 替换为250格式
	return strings.Replace(img, "_1280", "_250", -1)
}

// 获取图片数量
func Photos(p []models.Photo) string {
	// 获取数量
	nums := len(p)
	// 返回
	return fmt.Sprintf("%d", nums)
}
