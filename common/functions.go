package common

import (
	"git.wskehao.com/ylqjgm/Tumbex/models"
	"gopkg.in/mgo.v2/bson"
	"strings"
)

// 获取统计数据
func Count() map[string]int {
	// 定义数组
	count := make(map[string]int)
	// 获取博客统计
	count["Blog"] = models.GetBlogCount()
	// 获取视频统计
	count["Video"] = models.GetVideoCount()
	// 获取图片统计
	count["Photo"] = models.GetPhotoCount()
	return count
}

// 关键字搜索博客
func SearchByTag(key string, page, limit int) ([]models.Blog, int, error) {
	return models.GetBlogsByTag(key, page, limit)
}

// 获取博客信息
func GetBlogByApi(name string) (blog models.Blog, videos int, photos int, err error) {
	// 从API获取视频博客信息
	t, err := GetApi(name, "video", 0, 1)
	// 如果出错
	if nil != err {
		return
	}
	// 获取博客信息
	blog = t.GetBlog()
	// 写入博客数据
	err = blog.Create()
	// 如果出错
	if nil != err {
		return
	}
	// 获取视频数量
	videos = t.GetTotals()
	// 获取图片博客信息
	t, err = GetApi(name, "photo", 0, 1)
	// 获取图片数量
	photos = t.GetTotals()
	return
}

// 获取博客信息但不写入
func GetBlogNotWrite(name, ptype string) (blog models.Blog, totals int, err error) {
	// 从API获取博客信息
	t, err := GetApi(name, ptype, 0, 1)
	// 如果出错
	if nil != err {
		return
	}
	// 获取博客信息
	blog = t.GetBlog()
	// 获取文章数量
	totals = t.GetTotals()
	return
}

// 从数据库获取博客信息
func GetBlogByDb(blogId bson.ObjectId) (models.Blog, error) {
	return models.GetBlogById(blogId)
}

// 检查博客文章对比
func CheckPost(blog models.Blog, ptype bool, totals int) bool {
	// 从数据库获取文章统计
	cnt := models.GetPostCount(blog.Id_, ptype)
	// 对比
	return cnt == totals
}

// 获取文章列表并写入数据库
func GetAndWritePosts(blog models.Blog, ptype string, start, limit int) error {
	// 从API获取文章列表
	t, err := GetApi(blog.Name, ptype, start, limit)
	// 是否出错
	if nil != err {
		return err
	}
	// 获取文章列表
	posts, tags := t.GetPosts()
	// 循环写入文章数据
	for _, post := range posts {
		// 设置博客编号
		post.BlogId = blog.Id_
		// 写入文章
		post.Create()
		// 如果是图片
		if post.PostType {
			// 循环图片列表
			for _, photo := range post.Photos {
				// 写入图片
				photo.Create()
			}
		} else {
			// 写入视频
			post.Video.Create()
		}
	}
	// 循环写入标签数据
	for _, tag := range tags {
		// 写入标签数据
		tag.Create()
		// 组合关系数据
		relation := models.Relation{
			BlogId: blog.Id_,
			TagId:  tag.Id_,
		}
		// 写入关系
		relation.Create()
	}
	return nil
}

// 获取视频列表
func GetVideos(blogId bson.ObjectId, page int) (posts []models.Post, err error) {
	// 获取文章列表
	posts, err = models.GetPosts(blogId, false, (page-1)*20)
	// 是否出错
	if nil != err {
		return
	}
	// 循环文章列表
	for i, _ := range posts {
		// 获取视频信息
		video, err := models.GetVideo(posts[i].PostId)
		// 是否出错
		if nil != err {
			continue
		}
		// 加入到文章中
		posts[i].Video = video
	}
	return
}

// 获取视频总量
func GetVideoCount(blogId bson.ObjectId) int {
	return models.GetPostCount(blogId, false)
}

// 获取图片列表
func GetPhotos(blogId bson.ObjectId, page int) (posts []models.Post, err error) {
	// 获取文章列表
	ps, err := models.GetPosts(blogId, true, (page-1)*20)
	// 是否出错
	if nil != err {
		return
	}
	// 循环文章列表
	for _, post := range ps {
		// 获取图片列表信息
		photos, err := models.GetPhotos(post.PostId)
		// 是否出错
		if nil != err {
			continue
		}
		// 若图片不存在
		if 0 == len(photos) {
			// 跳过本次循环
			continue
		}
		// 替换列表
		post.Photos = photos
		// 加入列表
		posts = append(posts, post)
	}
	return
}

// 获取图片总量
func GetPhotoCount(blogId bson.ObjectId) int {
	return models.GetPostCount(blogId, true)
}

// 获取图片列表Json
func GetPhotosJson(postId int64) (json models.JsonData, err error) {
	// 获取图片列表
	photos, err := models.GetPhotos(postId)
	// 如果出错
	if nil != err {
		return
	}
	// 循环图片列表
	for i, photo := range photos {
		// 图片结构体
		data := models.PhotoData{
			Alt:   i,
			Pid:   i,
			Src:   photo.Url,
			Thumb: strings.Replace(photo.Url, "_1280", "_250", -1),
		}
		// 加入Json中
		json.Data = append(json.Data, data)
	}
	return
}
